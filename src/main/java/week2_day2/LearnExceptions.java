package week2_day2;

public class LearnExceptions {
	
	public static void main(String[] args) {
		int a[] = {1,3,4,5,6};
		try {
		System.out.println(a[5]);
		}
		catch (ArrayIndexOutOfBoundsException e){
			System.out.println("Invalid Position");
		}
		
	}
}
