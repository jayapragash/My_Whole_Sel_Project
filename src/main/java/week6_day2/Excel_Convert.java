package week6_day2;

import java.io.IOException;
import java.lang.reflect.Array;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel_Convert {

	public static Object[][] ExcelRead(String className) throws IOException {
		XSSFWorkbook workBook = new XSSFWorkbook("./data/"+className+".xlsx");
		XSSFSheet sheet = workBook.getSheetAt(0);
		int lastRowNum = sheet.getLastRowNum();
		int lastCellNum = sheet.getRow(0).getLastCellNum();
		Object[][] data = new Object[lastRowNum][lastCellNum];
		for (int j = 1; j <= lastRowNum ; j++) {
			XSSFRow row = sheet.getRow(j);
			for (int i = 0; i < lastCellNum; i++) {
				XSSFCell cell = row.getCell(i);
				String stringCellValue = cell.getStringCellValue();
				data[j-1][i] = stringCellValue;
				System.out.println(stringCellValue);
			}  
		}
		workBook.close();
		return data;

	}

}


