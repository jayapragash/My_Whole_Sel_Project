package week6_day2;


import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import week6_day2.LoginMethod;


public class CW_Create_Lead extends LoginMethod{


	@BeforeClass
	public void Data1() {
		TCName = "Create Lead";
		Description = "This will create a lead";
		Author = "Pragash";
		Category = "Usability Testing";
		FileName = "createlead";
	}

	@Test(dataProvider = "fetchdata")
	@Parameters("createlead")
	public void CreateLead(String CName, String Name, String LName, String PNo, String Eid) {		
		WebElement crmSfa = locateElement("LinkText","CRM/SFA");
		click(crmSfa);

		WebElement createLead = locateElement("LinkText", "Create Lead");
		click(createLead);

		WebElement companyName = locateElement("id", "createLeadForm_companyName");
		type(companyName, CName);

		WebElement firstName = locateElement("id", "createLeadForm_firstName");
		type(firstName, Name);

		WebElement lastName = locateElement("id", "createLeadForm_lastName");
		type(lastName, LName);

		WebElement source = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingIndex(source, 1);

		WebElement marketingCompaign = locateElement("id", "createLeadForm_marketingCampaignId");
		selectDropDownUsingText(marketingCompaign, "Automobile");

		WebElement primePhoneNumber = locateElement("id", "createLeadForm_primaryPhoneNumber");
		type(primePhoneNumber, PNo);

		WebElement primeEmailId = locateElement("id", "createLeadForm_primaryEmail");
		type(primeEmailId, Eid);

		WebElement createLeadClick = locateElement("class", "smallSubmit");
		click(createLeadClick);
		System.out.println("Create Lead completed");


	}
	
}
