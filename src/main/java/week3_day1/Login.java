package week3_day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

//import org.openqa.selenium.chrome.ChromeDriver;

public class Login {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("HCL Technologies");
		driver.findElementById("createLeadForm_firstName").sendKeys("Jayapragash");
		driver.findElementById("createLeadForm_lastName").sendKeys("K V");
		
		//To click a Drop Down of Source
		WebElement dD1 = driver.findElementById("createLeadForm_dataSourceId");
		Select source_DD = new Select(dD1);
		source_DD.selectByValue("LEAD_EMPLOYEE");
		
		//To click a Drop Down of Marketing Campaign
		WebElement dD2 = driver.findElementById("createLeadForm_marketingCampaignId");
		Select Marketing_DD = new Select(dD2);
		Marketing_DD.selectByVisibleText("Road and Track");
		
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Jay");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("V");
		driver.findElementById("createLeadForm_personalTitle").sendKeys("Mr.");
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Senior Software Engineer");
		driver.findElementById("createLeadForm_departmentName").sendKeys("Testing");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("3,60,000");
		
		WebElement dD3 = driver.findElementById("createLeadForm_currencyUomId");
		Select Currency_DD = new Select(dD3);
		Currency_DD.selectByValue("INR");
		
		WebElement dD4 = driver.findElementById("createLeadForm_industryEnumId");
		Select EnumId_DD = new Select(dD4);
		EnumId_DD.selectByVisibleText("Computer Software");
		
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("15Lakh");
		
		WebElement dD5 = driver.findElementById("createLeadForm_ownershipEnumId");
		Select Owner_DD = new Select(dD5);
		Owner_DD.selectByValue("OWN_CCORP");
		
		driver.findElementById("createLeadForm_sicCode").sendKeys("605056");
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("Yes");
		driver.findElementById("createLeadForm_description").sendKeys("I'm happy with thit industry");
		driver.findElementById("createLeadForm_importantNote").sendKeys("Nothing");
		driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("+91");
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("0413");
		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("2244469");
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("44469");
		driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("Number");
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("jayapagash008@gmail.com");
		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("www.hcl.com");
		
		driver.findElementById("createLeadForm_generalToName").sendKeys("Jayapragash");
		driver.findElementById("createLeadForm_generalAttnName").sendKeys("Pragash");
		driver.findElementById("createLeadForm_generalAddress1").sendKeys("First Street");
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("New Saram");
		driver.findElementById("createLeadForm_generalCity").sendKeys("Puducherry");
		
		WebElement dD6 = driver.findElementById("createLeadForm_generalCountryGeoId");
		Select Country_DD = new Select(dD6);
		Country_DD.selectByValue("IND");
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		WebElement dD7 = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
		Select State_DD = new Select(dD7);
		State_DD.selectByValue("IN-TN");
		
		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("605013");
		driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("91");
		driver.findElementByClassName("smallSubmit").click();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.close();
		System.out.println("Successfully Executed the above code");
	}

}
