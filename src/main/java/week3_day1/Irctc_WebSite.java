package week3_day1;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.utils.FileUtil;

public class Irctc_WebSite {
	public static void main(String[] args) throws IOException {
		System.setProperty("webdriver.chrome.driver","./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByLinkText("AGENT LOGIN").click();
		driver.findElementByLinkText("Sign up").click();
		driver.findElementByName("userRegistrationForm:userName").sendKeys("jayapragash008");
		driver.findElementByName("userRegistrationForm:checkavail").click();
		//Alert alert = driver.switchTo().alert();
		//alert.accept();
		driver.findElementByName("userRegistrationForm:password").sendKeys("New1234@123");
		driver.findElementByName("userRegistrationForm:confpasword").sendKeys("New1234@123");
		
		WebElement dD = driver.findElementByName("userRegistrationForm:securityQ");
		Select Security = new Select(dD);
		Security.selectByValue("0");
		
		driver.findElementByName("userRegistrationForm:securityAnswer").sendKeys("Jay");
		driver.findElementByName("userRegistrationForm:firstName").sendKeys("Jayapragash");
		driver.findElementByName("userRegistrationForm:lastName").sendKeys("K V");
		
		WebElement dD1 = driver.findElementById("userRegistrationForm:dobDay");
		Select Date = new Select(dD1);
		Date.selectByValue("08");
		
		WebElement dD2 = driver.findElementById("userRegistrationForm:dobMonth");
		Select Month = new Select(dD2);
		Month.selectByVisibleText("SEP");
		
		WebElement dD3 = driver.findElementById("userRegistrationForm:dateOfBirth");
		Select Year = new Select(dD3);
		Year.selectByVisibleText("1992");
		
		WebElement dd4 = driver.findElementById("userRegistrationForm:occupation");
		Select Occupation = new Select(dd4);
		Occupation.selectByVisibleText("Private");
		
		WebElement dD5 = driver.findElementById("userRegistrationForm:countries");
		Select Country = new Select(dD5);
		Country.selectByVisibleText("India");
		
		driver.findElementById("userRegistrationForm:email").sendKeys("jayapragash008@gmail.com");
		
		File Source = driver.getScreenshotAs(OutputType.FILE);
		File Destintion = new File("D:\\");
		FileUtils.copyFile(Source, Destintion);
				
		
		//driver.close();
	}

}
