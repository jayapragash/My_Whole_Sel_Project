package week2_day1;
import java.util.*;

public class TV_Set_Learn {
	//Want to print in ASCII order (ie) Alphatic order - Use class TreeSet()
	//Want to print in Random order - Use Class HashSet()
	//Want to print in the order that the data is inserted - Use Class LinkedHashSet()
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Set<String> Tvs = new TreeSet<String>();
		Tvs.add("Samsung");
		Tvs.add("Panasonic");
		Tvs.add("Onida");
		Tvs.add("Sony");
		Tvs.add("Onida");
		
		System.out.println("The TV List : " );
		for (String tvs1 : Tvs) {
			System.out.println(tvs1);
			
		}
List<String> brand =new ArrayList<String>();
brand.addAll(Tvs);
System.out.println("The Last position is : " + brand.get(3));
	}

}