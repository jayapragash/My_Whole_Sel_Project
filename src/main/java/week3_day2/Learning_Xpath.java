package week3_day2;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Learning_Xpath {
	public static void main(String[] args) {
		
	System.setProperty("webdriver.chrome.driver","./driver/chromedriver.exe");
	ChromeDriver driver = new ChromeDriver();
	driver.manage().window().maximize();
	driver.get("http://leaftaps.com/opentaps/");
	driver.findElementById("username").sendKeys("DemoSalesManager");
	driver.findElementById("password").sendKeys("crmsfa");
	driver.findElementByClassName("decorativeSubmit").click();
	driver.findElementByLinkText("CRM/SFA").click();
	
	//To find first Lead ID
	driver.findElementByLinkText("Leads").click();
	driver.findElementByLinkText(("(//div[@class='x-grid3-scroller']//a)[1]"));
	
	//To find Lead Button under find leads tag
	driver.findElementByLinkText("Find Leads").click();
	driver.findElementByXPath("(//button[@class='x-btn-text'])[7]");
	
	//To find second image under merge lead
	driver.findElementByLinkText("Merge Leads");
	driver.findElementByXPath("//input[@id='partyIdTo']/../a/img");
	driver.close();
	}
}
