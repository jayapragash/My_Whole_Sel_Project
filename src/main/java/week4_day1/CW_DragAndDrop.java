package week4_day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class CW_DragAndDrop {
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("http://jqueryui.com/");
		driver.findElementByLinkText("Draggable").click();;
		driver.switchTo().frame(driver.findElementByClassName("demo-frame"));
		WebElement draggable = driver.findElementByXPath("//div[@id='draggable']/p");
		Actions builder = new Actions(driver);
		builder.dragAndDropBy(draggable, 227, 223).perform();
		//Thread.sleep(5000);
		
	}
}
