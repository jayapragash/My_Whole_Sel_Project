package week3_day2;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Working_With_Table {
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://erail.in/");
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MDU", Keys.TAB);
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("MAS", Keys.TAB);
		
		WebElement quota = driver.findElementById("cmbQuota");
		Select quo = new Select(quota);
		quo.selectByValue("SS");
		
		WebElement Class = driver.findElementById("selectClassFilter");
		Select class1 = new Select(Class);
		class1.selectByValue("0");
		
		driver.findElementById("buttonFromTo");
		//driver.close();
		
		WebElement table = driver.findElementByXPath("//table[@class='DataTable TrainList']");
		List<WebElement> Rows = table.findElements(By.tagName("tr"));
//		
//		for (WebElement rows : Rows) {
//			String rowss = rows.getText();
//			System.out.println(rowss);
//		}
		WebElement Row2 = Rows.get(1);
		List<WebElement> Column = Row2.findElements(By.tagName("td"));
		System.out.println(Column.get(1).getText());
		
		
		
		//driver.close();
		System.out.println("Exeuted Successfully");
				}

}
