package week2_day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HWAlphabaticOder {
	public static void main(String[] args) {
		String s = "i learnt a lot today";
		char c[] = s.toCharArray();
		List<Character> a = new ArrayList<Character>();
		for (int i=0;i<c.length;i++)
		{
			a.add(c[i]);
		}
		Collections.sort(a);
		for (Character character : a) {
			System.out.println(character);
		}
		int n=a.size();
		System.out.println("\n"+n);
		System.out.println("The last alphabet is "+a.get(n-1));
	}

}
