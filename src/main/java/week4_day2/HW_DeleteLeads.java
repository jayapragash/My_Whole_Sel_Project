package week4_day2;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ByTagName;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class HW_DeleteLeads extends LoginMethod{
	@Test
	public void editLead() throws InterruptedException {
		
		startApp("Chrome", "http://leaftaps.com/opentaps/control/main");
		WebElement userName = locateElement("id", "username");
		type(userName, "DemoSalesManager");
		
		WebElement passWord = locateElement("id","password");
		type(passWord, "crmsfa");
		
		WebElement logIn = locateElement("class", "decorativeSubmit");
		click(logIn);
		
		WebElement crmSfa = locateElement("LinkText","CRM/SFA");
		click(crmSfa);
		
		WebElement leadsLink = locateElement("LinkText", "Leads");
		click(leadsLink);
		
		WebElement findLeads = locateElement("LinkText", "Find Leads");
		click(findLeads);
		
		WebElement Phone = locateElement("xpath", "(//span[@class='x-tab-strip-text '])[2]");
		click(Phone);
		
		WebElement FindLeads1 = locateElement("xpath", "(//button[@class='x-btn-text'])[7]");
		click(FindLeads1);
		
		WebElement Table1 = locateElement("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]");
		//List<WebElement> Row = Table1.findElements(By.tagName("tr"));
		//WebElement Row_Coll = Row.get(1);
		String text = getText(Table1);
		System.out.println(text);
		
		WebElement Find = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]");
		click(Find);
		
		WebElement Delete = locateElement("xpath", "(//div[@class='frameSectionExtra']/a)[4]");
		click(Delete);
		
		WebElement findLeads1 = locateElement("LinkText", "Find Leads");
		click(findLeads1);
		
		closeBrowser();
	}

}
