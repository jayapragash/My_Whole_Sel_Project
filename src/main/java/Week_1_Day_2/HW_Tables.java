package Week_1_Day_2;
import java.util.Scanner;

public class HW_Tables {
	public static void main (String args[]) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter till what number the tables should continue ");
		int a = s.nextInt();
		//int i = 1;
		System.out.println("Enter on what table the output should be ");
		int b = s.nextInt();
		for (int i=1;i<=a;i++) {
			System.out.println(i + " " + b + " = " + i*b);
		}
		}
}
