package Week_1_Day_1;

public class Mobile_Phone {

	long mobile_No;
	String Make;
	float Price=1256.50f;
	int Camera_Mega_Pixel;
	boolean is_Dual_Sim_Enabled;
	char Four_G_Enabled;
	
	void Assign_Variable() {
		mobile_No = 1234567890l;
		Make = "Lenovo";
		Price = 12500.50f;
		Camera_Mega_Pixel = 12;
		is_Dual_Sim_Enabled = false;
		Four_G_Enabled = 'y';
	}
	
	public static void Lock(long mobile_No) {
		System.out.print(mobile_No + " is locked");
	}
		
	static void Add_Contact(long Mob_Number,String Name) {
		System.out.println("\n" + Mob_Number + " is added to the Name " + Name);
	}
	
	public static long Call(long Mob_Number) {
		System.out.println("\n" + "Calling the Number " + Mob_Number);
		return Mob_Number;
	}
	
	public static void SMS( long To_Mob_No, String Mess_To_Send) {
		//System.out.println("Hi i\'m Jayapragash");
		System.out.println("The message "+Mess_To_Send+ " number to " + To_Mob_No);
		//return Mess_To_Send;
	}
	
	public static void Edit_Contact (long Old_Number, long New_Number, String Name) {
		System.out.println("\nThe Old Number "+Old_Number+" is deleted");
		System.out.println("The New Number "+New_Number+" is Added to Contact "+Name);
	}
	
	static long Search_Contact(String Name) {
		System.out.println("\nThe Search Contact for the name "+Name);
	   return 12345;
	}
	
	/*public static void main(String args[]) {
		Lock(8526945648l);
		Add_Contact(9600820394l,"Pragash");
		long a = Call(9600820394l);
		System.out.println(a);
		String sms = SMS(9600820394l,124678978l,"Message has been sent");
		System.out.println(sms);
		Edit_Contact(960082094l,9600825696l,"Pragash");
		Search_Contact("Pragash",960082596l);
	}*/
}
