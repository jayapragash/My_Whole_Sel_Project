package week2_day2;
import java.util.*;
import java.util.Map.Entry;
public class Cw_Sets_TV {
	public static void main(String[] args) {
		Map<String, Integer> Tvs = new HashMap<String, Integer>();
		Tvs.put("Samsung", 4);
		Tvs.put("Onida", 5);
		Tvs.put("Sony", 10);
		int total=0;
		for (Entry<String, Integer> string : Tvs.entrySet()) {
			System.out.println(string.getValue());
			total = total+string.getValue();			
		}
		System.out.println("\n\nTotal Number of TV "+total);
	}
}
