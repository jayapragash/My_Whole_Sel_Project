package wdMethods;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.sound.midi.SysexMessage;

import org.apache.commons.io.FileUtils;
import org.apache.poi.util.SystemOutLogger;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import Week_1_Day_1.Learning_Object;
import okhttp3.internal.cache.DiskLruCache.Snapshot;
import week4_day2.Learning_Reports;

public class SeMethods extends Learning_Reports implements WdMethods{
	public int i = 1;
	public RemoteWebDriver driver;
	private String text;
	public void startApp(String browser, String url) {
		try {
		if(browser.equalsIgnoreCase("chrome")){
				System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
				driver = new ChromeDriver();
			} else if (browser.equalsIgnoreCase("firefox")){
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			Result("The Browser "+browser+" has been launched", "pass");
		} catch (WebDriverException e) {
			Result("The Browser "+browser+" has not been launched", "Fail");
		} finally {
			takeSnap();
		}

	}

	
	public WebElement locateElement(String locator, String locValue) {
		try {
			switch(locator) {
			case "id"	 : return driver.findElementById(locValue);
			case "class" : return driver.findElementByClassName(locValue);
			case "xpath" : return driver.findElementByXPath(locValue);
			case "LinkText" : return driver.findElementByLinkText(locValue);
			case "name" : return driver.findElementByName(locValue);
			case "tagname" : return driver.findElementByTagName(locValue);
			}
		} catch (NoSuchElementException e) {
			Result("The locater "+locator+" is available", "pass");
		} catch (Exception e) {
			Result("The locater "+locator+" is not available", "pass");
		}
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public void type(WebElement ele, String data) {
		try {
			ele.sendKeys(data);
			Result("The Data has been entered", "Pass");
		} catch (WebDriverException e) {
			Result("WebDriverException is thrown in type Method", "fail");
		} finally {
			takeSnap();
		}
	}

	
	public void clickWithNoSnap(WebElement ele) {
		try {
			ele.click();
			Result("The Button has been clicked", "pass");
		} catch (Exception e) {
			Result("The Button is not clicked", "fail");
		}
	}
	
	
	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			Result("The element "+ele+" has been clicked successfully", "pass");
		} catch (WebDriverException e) {
			Result("The element "+ele+" has not been clicked successfully", "fail");
		} finally {
			takeSnap();
		}
	}

	@Override
	public String getText(WebElement ele) {
		String text=null;
		try {
			text = ele.getText();
			Result("The text "+text+" has been got by the browser", "pass");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Result("The text "+ele+" is not available", "fail");
		}
		return text;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select dd = new Select(ele);
			dd.selectByVisibleText(value);
			System.out.println("The DropDown Is Selected with VisibleText "+value);
		} catch (Exception e) {
			System.err.println("The DropDown Is not Selected with VisibleText "+value);
		} finally {
			takeSnap();
		}

	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try {
			Select dd = new Select(ele);
			dd.selectByIndex(index);
			System.out.println("The DropDown is selected with Index "+index);
		} catch (Exception e) {
			System.err.println("The Drop down is not selected with the Index "+index);
		}finally {
			takeSnap();
		}

	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		String verifyTitle = driver.getTitle();
		try {
			if(verifyTitle.equals(expectedTitle))
			{
				System.out.println("The Title "+expectedTitle+" is verified");
			}else
			{
				System.out.println("The Title "+expectedTitle+" is not verified");
			}
		} catch (Exception e) {
			System.out.println("Something Went wrong");
		}
		return false;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		try {
			String text = ele.getText();
			boolean contentEquals = text.contains(expectedText);
			
			if (contentEquals == true) {
				System.out.println("The given Value "+contentEquals+" is Verified");
			}else
			{
				System.out.println("The given Value "+contentEquals+" did not match");
			}
		} catch (Exception e) {
			System.out.println("Something Went wrong");
		}

	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		try {
			String text = ele.getText();
			if(text.contains(expectedText))
			{
				System.out.println("The partial text is verified");
			}else {
				System.out.println("The Partial Text is failed");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Something went wrong with Partialtext");
		}
		finally {
			takeSnap();
		}
	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		String Strvalue = ele.getAttribute(attribute);
		try {
			if (Strvalue.equals(value)) {
				System.out.println("The Given Attribute Value "+Strvalue+" is available");
			}else {
				System.out.println("The Given Attribute Value "+Strvalue+" is not available");
			}
		} catch (NoSuchElementException e) {
			// TODO Auto-generated catch block
			System.out.println("The element is not available under VerifyPartialAttribute");
		}catch (Exception e) {
			System.out.println("Something went wrong in VerifyPartialAttribute");
		}finally {
			takeSnap();
		}
		
		

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		String Strvalue = ele.getAttribute(attribute);
		try {
			if (Strvalue.contains(value)) {
				System.out.println("The Given Attribute Value "+Strvalue+" is available");
			}else {
				System.out.println("The Given Attribute Value "+Strvalue+" is not available");
			}
		} catch (NoSuchElementException e) {
			// TODO Auto-generated catch block
			System.out.println("The element is not available under VerifyPartialAttribute");
		}catch (Exception e) {
			System.out.println("Something went wrong in VerifyPartialAttribute");
		}finally {
			takeSnap();
		}
	}

	@Override
	public void verifySelected(WebElement ele) {
		try {
			String text = ele.getText();
			System.out.println(text);
			if(ele.isSelected() == true) {
				System.out.println("The Radio button of "+text+" is selected");
			}else {
				System.out.println("The Radio button of "+text+" is not selected");
			}
		} catch (Exception e) {
			System.out.println("Radio button is not available or some problem with it");
		}

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		try {
			boolean displayed = ele.isDisplayed();
			if (displayed == true)
			{
				System.out.println("The Value is displayed");
			}else
			{
				System.out.println("The value is not displayed");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Exception thrown at verifyDisplay ");
		}finally {
			takeSnap();
		}

	}

	@Override
	public void switchToWindow(int index) {
		try {
			Set<String> Window = driver.getWindowHandles();
			System.out.println("The size of windows is "+Window.size());
			List<String> NewWindow = new ArrayList<String>();
			NewWindow.addAll(Window);
			
			if(index !=0) {
				String SwitchWindow = NewWindow.get(index);
				driver.switchTo().window(SwitchWindow);
				Result("The Required Windows "+index+" is available", "Pass");
			} else if(index == 0) {
				String SwitchWindow = NewWindow.get(index);
				driver.switchTo().window(SwitchWindow);
				Result("The Parent Windows "+index+" is available", "Pass");
			}
			
		} catch (NoSuchWindowException e) {
			// TODO Auto-generated catch block
			Result("The Required Windows "+index+" is not available", "Fail");
		}
	}

	@Override
	public void switchToFrame(WebElement ele) {
		try {
			driver.switchTo().frame(ele);
			Result("The Frame is available and it is switched to required window", "pass");
			
		} catch (NoSuchFrameException e) {
			// TODO Auto-generated catch block
			Result("The Frame is not available", "fail");
		}finally {
			takeSnap();
		}

	}

	@Override
	public void acceptAlert() {
		try {
			driver.switchTo().alert().accept();
			Result("The Alert has been handled", "Pass");
		} catch (NoAlertPresentException e) {
			// TODO Auto-generated catch block
			Result("The Alert is not available", "fail");
			takeSnap();
		} catch(Exception e)
		{
			Result("The Alert is not available because of unknown exception", "fail");
		}

	}

	@Override
	public void dismissAlert() {
		try {
			driver.switchTo().alert().dismiss();
			Result("The Alert has been dimissed", "Pass");
		} catch (NoAlertPresentException e) {
			// TODO Auto-generated catch block
			Result("The Alert is not available", "fail");
			takeSnap();
		}catch(Exception e)
		{
			Result("The Alert is not available because of unknown exception", "fail");
		}

	}

	@Override
	public String getAlertText() {
		try {
			System.out.println("This is a popup");
			driver.switchTo().alert().accept();
			Result("The Alert has been handled and the message is sent", "Pass");
		} catch (NoAlertPresentException e) {
			// TODO Auto-generated catch block
			Result("The Alert is not available", "fail");
		}catch(Exception e)
		{
			Result("The Alert is not available because of unknown exception", "fail");
		}
		return null;
	}

	
	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./snaps/img"+i+".png");
		try {
			FileUtils.copyFile(src, des);
		} catch (IOException e) {
			System.err.println("IOException");
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		driver.close();
	}

	@Override
	public void closeAllBrowsers() {
		driver.quit();

	}




}
