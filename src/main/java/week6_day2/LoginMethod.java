package week6_day2;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import wdMethods.SeMethods;
import week6_day2.Excel_Convert;

public class LoginMethod extends SeMethods{
	@DataProvider(name = "fetchdata")
	public Object[][] data() throws IOException{
		Object[][] excelData = Excel_Convert.ExcelRead(FileName);
		return excelData;
	}
	
	@BeforeMethod
	@Parameters({"browser","url","uName","password"})
	public void Login(String browserOpen, String url, String uName, String Password){
		startApp(browserOpen, url);
		WebElement userName = locateElement("id", "username");
		type(userName, uName);
		
		WebElement passWord = locateElement("id","password");
		type(passWord, Password);
		
		WebElement logIn = locateElement("class", "decorativeSubmit");
		click(logIn);
	}
	
	@AfterMethod
	public void Close() {
		closeAllBrowsers();
	}
}
