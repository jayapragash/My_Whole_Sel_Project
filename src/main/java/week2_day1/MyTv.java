package week2_day1;

public class MyTv {
	public static void main(String args[])
	{
		SamsungTv newTv = new SamsungTv();
		newTv.switchOn(true);
		newTv.switchChannel('>');
		newTv.switchChannel(520);
		newTv.volume(50);
	}
}
