package week4_day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Cw_Learning_Alert_Frams {
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		
		//switch to frame
		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[text()='Try it']").click();
		
		//switch to alert
		driver.switchTo().alert().sendKeys("Pragash");
		driver.switchTo().alert().accept();
		String printElement = driver.findElementById("demo").getText();
		System.out.println(printElement);
		driver.close();
	}
}
