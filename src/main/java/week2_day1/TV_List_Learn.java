package week2_day1;
import java.util.*;
import java.util.List;

public class TV_List_Learn {
	public static void main(String[] args) {
		List<String> Tvs = new ArrayList<String>();
		Tvs.add("Samsung");
		Tvs.add("Panasonic");
		Tvs.add("Onida");
		Tvs.add("Sony");
		Tvs.add("Onida");
		System.out.println("The List of Tvs are ");
		for (String listOfTv : Tvs) {
			System.out.println(listOfTv);
		}
		System.out.println("Number of TVs in the list "+Tvs.size());
		Tvs.remove("Onida");
		for (String listOfTv : Tvs) {
			System.out.println(listOfTv);
		}
		Tvs.remove(3);
		System.out.println("\n\nThe New List is ");
		for (String listOfTv : Tvs) {
			System.out.println(listOfTv);
		}
		Collections.sort(Tvs);
		System.out.println("\n\n The ordered list are ");
		for (String List1 : Tvs) {
			System.out.println(List1);
		}
		System.out.println("Number of TVs in the list "+Tvs.size());
	}
}
