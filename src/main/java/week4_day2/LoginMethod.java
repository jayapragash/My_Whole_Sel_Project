package week4_day2;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;

import wdMethods.SeMethods;
import week6_day2.Excel_Convert;

public class LoginMethod extends SeMethods{
	
	@BeforeMethod(groups = "Common")
	public void Login(){
		startApp("Chrome", "http://leaftaps.com/opentaps/control/main");
		WebElement userName = locateElement("id", "username");
		type(userName, "DemoSalesManager");
		
		WebElement passWord = locateElement("id","password");
		type(passWord, "crmsfa");
		
		WebElement logIn = locateElement("class", "decorativeSubmit");
		click(logIn);
	}
	
	@AfterMethod(groups = "Common")
	public void Close() {
		closeAllBrowsers();
	}
}
