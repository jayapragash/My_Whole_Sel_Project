package week4_day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class HW_MergeLead extends SeMethods{
	
	@BeforeTest
	public void Data1() {
		TCName = "Create Lead";
		Description = "This will create a lead";
		Author = "Pragash";
		Category = "Usability Testing";
	}
	@Test
	public void DuplicateLead() throws InterruptedException {
		/*WebElement crmSfa = locateElement("LinkText","CRM/SFA");
		click(crmSfa);
		WebElement createLead = locateElement("LinkText", "Create Lead");
		click(createLead);*/
		WebElement clickLead = locateElement("linktext", "Leads");
		click(clickLead);
		WebElement clickMerge = locateElement("linktext", "Find Leads");
		click(clickMerge);
		WebElement clickEmail = locateElement("xpath", "//span[text()='Email']");
		click(clickEmail);
		WebElement eleEmail = locateElement("name", "emailAddress");
		type(eleEmail, "gopipj022@gmail.com");
		WebElement clickFindLeads = locateElement("xpath", "//button[text()='Find Leads']");
		click(clickFindLeads);
		Thread.sleep(3000);
		WebElement clickFirstele = locateElement("xpath", "(//a[@class='linktext'])[4]");
		click(clickFirstele);
		WebElement clickDupLeads = locateElement("xpath", "//a[text()='Duplicate Lead']");
		click(clickDupLeads);
		WebElement eleTitle = locateElement("id","createLeadForm_generalProfTitle");
		type(eleTitle, "Welcome Gopi");
		WebElement clickSubCreate = locateElement("name", "submitButton");
		click(clickSubCreate);
		WebElement getTitle = locateElement("id","viewLead_generalProfTitle_sp");
		getText(getTitle);
		verifyExactText(getTitle, "Welcome Gopi");



	}

}
