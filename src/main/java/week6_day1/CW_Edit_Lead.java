package week6_day1;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import week4_day2.LoginMethod;

public class CW_Edit_Lead extends LoginMethod{
	

	@BeforeClass
	public void Data1() {
		TCName = "Create Lead";
		Description = "This will create a lead";
		Author = "Pragash";
		Category = "Usability Testing";
	}
	@Test (dependsOnMethods = "week6_day1.CW_Create_Lead.CreateLead")
		public void editLead() throws InterruptedException {
			
			WebElement crmSfa = locateElement("LinkText","CRM/SFA");
			click(crmSfa);
			
			WebElement leadsLink = locateElement("LinkText", "Leads");
			click(leadsLink);
			
			WebElement findLeads = locateElement("LinkText", "Find Leads");
			click(findLeads);
			
			WebElement firstName = locateElement("xpath", "(//input[@name='firstName'])[3]");
			type(firstName, "MuthuKumari");
			
			WebElement findLeadbutton = locateElement("xpath", "//button[text()='Find Leads']");
			click(findLeadbutton);
			
			WebElement clickFirstLead = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]");
			click(clickFirstLead);
			
			WebElement editButton = locateElement("LinkText", "Edit");
			click(editButton);
			
			
			WebElement editCompanyName = locateElement("id", "updateLeadForm_companyName");
			editCompanyName.clear();
			type(editCompanyName, "Infy");
			
			WebElement updateButton = locateElement("xpath", "//input[@value='Update']");
			click(updateButton);
			
			WebElement verifyUpdatedText = locateElement("id", "viewLead_companyName_sp");
			verifyExactText(verifyUpdatedText, "Infy");
			
			Thread.sleep(3000);
			
		}
}
