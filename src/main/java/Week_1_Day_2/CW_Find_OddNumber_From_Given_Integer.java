package Week_1_Day_2;
import java.util.*;
public class CW_Find_OddNumber_From_Given_Integer {
	public static void main(String args[])
	{
		Scanner s = new Scanner(System.in);
		System.out.println("Enter a number: ");
		int Number = s.nextInt();
		while (Number > 0)
		{
			int number1 = Number%10;
			if (number1%2!=0)
			{
				System.out.println("the Even Numbers are "+number1);
			}
			Number = Number/10;
		}
	}

}
