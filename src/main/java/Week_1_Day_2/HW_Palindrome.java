package Week_1_Day_2;

import java.util.Scanner;

public class HW_Palindrome {
	public static void main(String args[])
	{
		int r,sum=0,temp;    
		System.out.println("Enter a number to check whether it is palindrome or not: ");
		Scanner s = new Scanner(System.in);
		int n = s.nextInt();  
		  
		  temp=n;    
		  while(n>0){    
		   r=n%10;  //getting remainder  
		   sum=(sum*10)+r;    
		   n=n/10;    
		  }    
		  if(temp==sum)    
		   System.out.println("palindrome number ");    
		  else    
		   System.out.println("not palindrome");    
		}  
	}

