package Week_1_Day_2;

import java.util.Scanner;

public class CW_While_With_Mobile_Balance {
	public static void main(String args[])
	{
		Scanner s = new Scanner(System.in);
		System.out.println("The Balance available in Sim is ");
		int Balance = s.nextInt();
		while (Balance > 0)
		{
			System.out.println("The message is sent");
			Balance = Balance-1;
			System.out.println("The Balance Now = "+Balance);
			System.out.println("Calling is over");
			Balance = Balance-2;
			System.out.println("The Balance Now = "+Balance);
		}
	}
}
