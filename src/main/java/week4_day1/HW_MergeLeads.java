package week4_day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.poi.poifs.property.Parent;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class HW_MergeLeads {
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver","./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Merge Leads").click();
		
		
		
		driver.findElementByXPath("(//table[@id='widget_ComboBox_partyIdFrom']/following::a)[1]").click();
		Set<String> noOfWindows = driver.getWindowHandles();
		List<String> Windows = new ArrayList<String>();
		Windows.addAll(noOfWindows);
		System.out.println(Windows.size());
		String newWindow = Windows.get(1);
		driver.switchTo().window(newWindow);
		
		
		
		
		driver.findElementByXPath("(//div[@class='x-form-element']/input)[1]").sendKeys("12");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		
		
//		WebElement table = driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]");
//		System.out.println("executed successfully");
//		List<WebElement> Row = table.findElements(By.className("x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first"));
//		WebElement Rows = Row.get(1);
//		List<WebElement> Column = Rows.findElements(By.tagName("td"));
//		Column.get(1).click();
		
		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]").click();
		driver.switchTo().window(Windows.get(0));
		driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[2]").click();
		System.out.println(Windows.size());
		for (String WinNuw : driver.getWindowHandles()) {
			driver.switchTo().window(WinNuw);
		}
		driver.findElementByXPath("//input[@class=' x-form-text x-form-field']").sendKeys("55");
		driver.findElementByXPath("(//button[@class='x-btn-text'])[1]").click();
		Thread.sleep(3000);
		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]").click();
		driver.switchTo().window(Windows.get(0));
		driver.findElementByClassName("buttonDangerous").click();
		driver.switchTo().alert().accept();
		driver.findElementByLinkText("Find Leads").click();
		

}

}