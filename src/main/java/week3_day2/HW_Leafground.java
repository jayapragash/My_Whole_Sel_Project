package week3_day2;

import java.util.List;

import org.apache.poi.util.SystemOutLogger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.UnexpectedTagNameException;

public class HW_Leafground {
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http:leafground.com/pages/Dropdown.html");
		
		WebElement dropdown1 = driver.findElementById("dropdown1");
		Select dd1 = new Select(dropdown1);
		dd1.selectByValue("1");
		
		WebElement dropdown2 = driver.findElementByName("dropdown2");
		Select dd2 = new Select(dropdown2);
		dd2.selectByVisibleText("Selenium");
		
		WebElement dropdown3 = driver.findElementById("dropdown3");
		Select dd3 = new Select(dropdown3);
		dd3.selectByVisibleText("Selenium");
		
		WebElement dropdown4 = driver.findElementByClassName("dropdown");
		Select dd4 = new Select(dropdown4);
		dd4.selectByVisibleText("Selenium");
		List<WebElement> elements = dd4.getOptions();
		for (WebElement options : elements) {
			System.out.println(options.getText());
		}
		System.out.println(elements.size());
		
		try {
			WebElement dropdown5 = driver.findElementByXPath("(//div[@class='example'])[5]");
			Select dd5 = new Select(dropdown5);
			dd5.selectByValue("1");
		} catch (UnexpectedTagNameException e) {
			// TODO Auto-generated catch block
			System.out.println("Tag name is wrong");
		}
		
		driver.findElementByXPath("//option[@disabled='true']/following::option").click();
		
	}
}
