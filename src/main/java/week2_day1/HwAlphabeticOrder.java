package week2_day1;
import java.util.*;
import java.util.stream.Collectors;

public class HwAlphabeticOrder {
	public static void main(String[] args) {
		String s = "amazon development center";
		char[] c = s.toCharArray();
		List<Character> a = new ArrayList<Character>();
		for(int i=0;i<c.length;i++)
		{
			a.add(c[i]);
			//System.out.println(c[i]);
		}
		Collections.sort(a);
		System.out.println("The ordered list are ");
		for (Character character : a) {
			System.out.println(character);
		}
	}

}
