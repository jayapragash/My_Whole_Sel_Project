package week6_day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Learning_Excel {
	public static void main(String[] args) throws IOException {
		XSSFWorkbook workBook = new XSSFWorkbook("./data/createlead.xlsx");
		XSSFSheet sheet = workBook.getSheet("createlead");
		int lastRowNum = sheet.getLastRowNum();
		int lastCellNum = sheet.getRow(0).getLastCellNum();
		for (int j = 1; j <= lastRowNum ; j++) {
			XSSFRow row = sheet.getRow(j);
			for (int i = 0; i < lastCellNum; i++) {
				XSSFCell cell = row.getCell(i);
				String stringCellValue = cell.getStringCellValue();
				System.out.println(stringCellValue);
			}  
		}
		workBook.close();
		
	}

}
