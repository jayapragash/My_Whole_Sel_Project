package week2_day1;
import java.util.*;

public class HW_Unique_Character {
	public static void main(String[] args) {
		String s = "cognizant";
		char c[] = s.toCharArray();
		Set<Character> newSet = new LinkedHashSet<Character>();
		for (int i=0;i<c.length;i++) {
			newSet.add(c[i]);
		}
		for (Character character : newSet) {
			System.out.println(character);
		}
	}
}
