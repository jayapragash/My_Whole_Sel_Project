package learning_DataProvider_And_Paramaeter;


import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;


public class CW_Create_Lead extends LoginMethod{

	
	@BeforeClass
	public void Data1() {
		TCName = "Create Lead";
		Description = "This will create a lead";
		Author = "Pragash";
		Category = "Usability Testing";
	}
	
	@Test(dataProvider = "CreateLead")
	@Parameters("CreateLead")
	public void CreateLead(String CName, String Name, String LName, String PNo, String Eid) {		
		WebElement crmSfa = locateElement("LinkText","CRM/SFA");
		click(crmSfa);
		
		WebElement createLead = locateElement("LinkText", "Create Lead");
		click(createLead);
		
		WebElement companyName = locateElement("id", "createLeadForm_companyName");
		type(companyName, CName);
		
		WebElement firstName = locateElement("id", "createLeadForm_firstName");
		//type(firstName, Namd);
		
		WebElement lastName = locateElement("id", "createLeadForm_lastName");
		type(lastName, LName);
		
		WebElement source = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingIndex(source, 1);
		
		WebElement marketingCompaign = locateElement("id", "createLeadForm_marketingCampaignId");
		selectDropDownUsingText(marketingCompaign, "Automobile");
		
		WebElement primePhoneNumber = locateElement("id", "createLeadForm_primaryPhoneNumber");
		type(primePhoneNumber, PNo);
		
		WebElement primeEmailId = locateElement("id", "createLeadForm_primaryEmail");
		type(primeEmailId, Eid);
		
		WebElement createLeadClick = locateElement("class", "smallSubmit");
		click(createLeadClick);
		System.out.println("Create Lead completed");
		
		
	}
	@DataProvider(name = "CreateLead")
	public Object[][] data(){
		Object[][] Data = new Object[2][5];
		Data[0][0] = "HCL";
		Data[0][1] = "Jayapragash";
		Data[0][2] = "V";
		Data[0][3] = "9894562863";
		Data[0][4] = "jayapragash@hcl.com";
		Data[1][0] = "Tata";
		Data[1][1] = "Gan";
		Data[1][2] = "N";
		Data[1][3] = "9894562855";
		Data[1][4] = "Gan@hcl.com";
		
		
		
		return Data;
		
	}
}
