package week2_day2;

public class LearnThrow {
	public void Division(int a, int b)
	{
		int c = a/b;
		System.out.println(c);
		if(b==0)
		{
		throw new ArithmeticException();
		}
	}
	
	public static void main(String[] args) {
		LearnThrow L = new LearnThrow();
		try {
		L.Division(10, 5);
		}catch(ArithmeticException e) {
			System.out.println("Dinominator cannot be 0");
		}
	}
}
