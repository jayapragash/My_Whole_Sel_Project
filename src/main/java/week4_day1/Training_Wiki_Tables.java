package week4_day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Training_Wiki_Tables {
	public static void main(String[] args) {
		//nowraplinks hlist collapsible autocollapse navbox-inner mw-collapsible mw-made-collapsible
		System.setProperty("webdriver.chrome.driver","./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://en.wikipedia.org/wiki/Lists_of_colors");
		//driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		WebElement table = driver.findElementByXPath("//table[@class='nowraplinks hlist collapsible autocollapse navbox-inner mw-collapsible mw-made-collapsible']");
		List<WebElement> Rows = table.findElements(By.tagName("tr"));
		String Values = Rows.get(1).getText();	
		WebElement Row = Rows.get(2);
		List<WebElement> Column = Row.findElements(By.tagName("td"));
		System.out.println(Column.get(1).getText());
		//System.out.println(Values);
		driver.close();
	}
}
