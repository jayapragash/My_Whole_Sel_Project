package Week_1_Day_2;
import java.util.*;

public class HW_Factorial {
	public static void main(String args[])
	{
		int Value=1;
		Scanner s = new Scanner(System.in);
		System.out.println("Enter a number to find a factorial of it: ");
		int Num = s.nextInt();
		for(int i=1;i<=Num;i++)
		{
			Value = Value*i;
		}
		System.out.println("The factorial of given number is: "+Value);
	}
}
