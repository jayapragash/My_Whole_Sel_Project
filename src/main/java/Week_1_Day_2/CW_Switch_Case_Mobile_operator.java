package Week_1_Day_2;

import java.util.Scanner;

public class CW_Switch_Case_Mobile_operator {
	public static void main(String args[]){
		Scanner s = new Scanner(System.in);
		System.out.println("Enter a first 3 digit of your phone number to find the Service Provider ");
		int Phone_Number = s.nextInt();
		switch (Phone_Number) {
		case 994:
			System.out.println("The Service Provider for "+Phone_Number+" is BSNL");
			break;
		case 900:
			System.out.println("The Service Provider for "+Phone_Number+" is Airtel");
			break;
		case 897:
			System.out.println("The Service Provider for "+Phone_Number+" is IDEA");
			break;
		case 630:
			System.out.println("The Service Provider for "+Phone_Number+" is JIO");
			break;
		default:
			System.out.println("Ther Service Provider for "+Phone_Number+" is not available");
		}
		}
		}
	