package week4_day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class CW_MergeLead extends LoginMethod{
	
	@BeforeTest
	public void Data1() {
		TCName = "Create Lead";
		Description = "This will create a lead";
		Author = "Pragash";
		Category = "Usability Testing";
	}

	@Test
	public void createLead() throws InterruptedException {
		WebElement crmSfa = locateElement("LinkText","CRM/SFA");
		click(crmSfa);
		WebElement clickLead = locateElement("LinkText", "Leads");
		click(clickLead);
		WebElement clickMerge = locateElement("LinkText", "Merge Leads");
		click(clickMerge);
		WebElement clickMerIcon = locateElement("xpath", "//table[@id='widget_ComboBox_partyIdFrom']/following-sibling::a/img");
		click(clickMerIcon);
		switchToWindow(1);
		System.out.println("Windows Pass1");
		
		
		WebElement typeID = locateElement("name", "id");
		type(typeID, "10551");
		WebElement buttonLead = locateElement("xpath", "//button[text()='Find Leads']");
		click(buttonLead);
		Thread.sleep(3000);
		WebElement clickFirstEle = locateElement("xpath", "(//a[@class='linktext'])[1]");
		clickWithNoSnap(clickFirstEle);
		Thread.sleep(3000);
		switchToWindow(0);
		System.out.println("Windows Pass0");
		Thread.sleep(2000);
		WebElement clickMerIcon1 = locateElement("xpath", "//table[@id='widget_ComboBox_partyIdTo']/following-sibling::a/img");
		clickWithNoSnap(clickMerIcon1);
		switchToWindow(1);
		System.out.println("Windows Pass");
		Thread.sleep(2000);
		
		WebElement typeIDTo = locateElement("name", "id");
		type(typeIDTo, "10888");
		System.out.println("typeIDTo PAss");
		WebElement buttonLeadTo = locateElement("xpath", "//button[text()='Find Leads']");
		click(buttonLeadTo);
		WebElement clickFirstEleTo = locateElement("xpath", "(//a[@class='linktext'])[1]");
		click(clickFirstEleTo);
		Thread.sleep(3000);
		WebElement clickMergeIcon = locateElement("xpath", "//a[text()='Merge']");
		click(clickMergeIcon);
		Thread.sleep(3000);
		acceptAlert();
		WebElement findLead = locateElement("xpath", "//a[text()='Find Leads']");
		type(findLead, "10551");
		click(buttonLead);
		WebElement getOutput = locateElement("xpath", "//div[text()='No records to display']");
		getText(getOutput);
		verifyExactText(getOutput, "No Records to display");
		
				
	}
}
