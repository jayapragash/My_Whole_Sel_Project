package week6_day1;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import week4_day2.LoginMethod;

public class CW_Create_Lead_Groups extends LoginMethod{

	
	@BeforeClass(groups = "Smoke")
	public void Data1() {
		TCName = "Create Lead";
		Description = "This will create a lead";
		Author = "Pragash";
		Category = "Usability Testing";
	}
	
	@Test(groups = "Smoke")
	public void CreateLead() {		
		WebElement crmSfa = locateElement("LinkText","CRM/SFA");
		click(crmSfa);
		
		WebElement createLead = locateElement("LinkText", "Create Lead");
		click(createLead);
		
		WebElement companyName = locateElement("id", "createLeadForm_companyName");
		type(companyName, "TCS");
		
		WebElement firstName = locateElement("id", "createLeadForm_firstName");
		type(firstName, "MuthuKumari");
		
		WebElement lastName = locateElement("id", "createLeadForm_lastName");
		type(lastName, "Murugan");
		
		WebElement source = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingIndex(source, 1);
		
		WebElement marketingCompaign = locateElement("id", "createLeadForm_marketingCampaignId");
		selectDropDownUsingText(marketingCompaign, "Automobile");
		
		WebElement primePhoneNumber = locateElement("id", "createLeadForm_primaryPhoneNumber");
		type(primePhoneNumber, "987456321");
		
		WebElement primeEmailId = locateElement("id", "createLeadForm_primaryEmail");
		type(primeEmailId, "muthukumari@gmail.com");
		
		WebElement createLeadClick = locateElement("class", "smallSubmit");
		click(createLeadClick);
		System.out.println("Create Lead completed");
		
		
	}
}
