package Week_1_Day_2;

public class CW_Use_String_Methods {
	public static void main(String args[])
	{
		String Word = "Jayapragash K V";
		//Using Length()
		int Length = Word.length();
		System.out.println("The Length of my name \"Jayapragash K V\" is "+Length);
		
		//Using tochararray
		char Character[] = Word.toCharArray();
		for(char name:Character)
		{
			System.out.println("The Array of my name \"Jayapragash K V\" is "+name);
		}
		
		//Using toLowerCase
		System.out.println("The Lowercase of my name \"Jayapragash K V\" is "+Word.toLowerCase());
		
		//Using toUpperCase
		System.out.println("The Lowercase of my name \"Jayapragash K V\" is "+Word.toUpperCase());
		
		//Using equals
		boolean Good = Word.equals("JayapragashKV");
		System.out.println("Is it Equal?"+Good);
		
		//Character At
		int Character_At = Word.charAt(11);
		System.out.println("The Character K is present in "+Character_At+" place of Jayapragash K V");
		
		
	}
}
