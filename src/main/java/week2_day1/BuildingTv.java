package week2_day1;

public interface BuildingTv {
	public void switchOn(boolean State);
	public void switchChannel(char Inc_Dec);
	public void volume(int vol);
	public void switchChannel(int chanNumber);
	
}
